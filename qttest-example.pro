greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app
DESTDIR = dist
OBJECTS_DIR = build
VERSION = 1.0
TARGET = qttest-example
QT += core gui
DEFINES += APP_VERSION=\\\"$$VERSION\\\"

CONFIG(debug, debug|release) {
    CONFIG += console
}

HEADERS += \
    mainwindow.h \
    mathtokenizer.h \
    token.h \
    mathparser.h

SOURCES += \
    main.cpp\
    mainwindow.cpp \
    mathtokenizer.cpp \
    mathparser.cpp

FORMS += \
    mainwindow.ui

test {
    message(Test build)

    QT += testlib
    TARGET = qttest-example-test
    SOURCES -= main.cpp

    HEADERS += \
        test/testmathparser.h \
        test/testmathtokenizer.h

    SOURCES += \
        test/main.cpp \
        test/testmathparser.cpp \
        test/testmathtokenizer.cpp
} else {
    message(Normal build)
}
