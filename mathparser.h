#ifndef MATHPARSER_H
#define MATHPARSER_H

#include <QtCore>
#include "token.h"

// LL(1) parser of simple mathematical expressions with standard operator priorities
// Grammar:
// A -> B + A | B - A | B
// B -> Int * B | Int / B | Int
class MathParser {
public:
    // Parses sequence of tokens as an integer mathematical expression and returns computed value, sets error in case of parsing error
    int parse(QList<Token> tokens, QString &error) const;

private:
    int parseAddSub(QList<Token> &tokens, QString &error) const;
    int parseMulDiv(QList<Token> &tokens, QString &error) const;
    int parseInt(QList<Token> &tokens, QString &error) const;
};

#endif // MATHPARSER_H
