#include <QApplication>

#include "mainwindow.h"

int main(int argc, char **argv) {
    QApplication app(argc, argv);

    app.setApplicationName(QObject::tr("QtTest example"));
    app.setApplicationVersion(QObject::tr(APP_VERSION));
    app.setOrganizationName(QObject::tr("Xilexio"));

    MainWindow w;
    w.show();

    return app.exec();
}
