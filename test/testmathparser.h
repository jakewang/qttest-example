#ifndef TESTMATHPARSER_H
#define TESTMATHPARSER_H

#include <QtTest>

// Test suite for MathParser
// Doesn't contain any comments or superfluous elements
class TestMathParser : public QObject {
    Q_OBJECT

private slots:
    void testEmptyFail();
    void testInt();
    void testInvalidOperator();
    void testAllOperators();
    void testDivisionByZeroFail();
    void testUnexpectedEndFail();
};

#endif // TESTMATHPARSER_H
